hatara-2.pdf: hatara.pdf
	pdftk $< $< cat output $@

hatara.pdf: hatara.tex
	xelatex $<

clean:
	$(RM) hatara.pdf hatara-2.pdf hatara.aux hatara.log
